/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recitation2;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.StackPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

/**
 *
 * @author Felix Rieg-Baumhauer
 * 
 */
public class Recitation2 extends Application {
    
    @Override
    public void start(Stage primaryStage) {
        Button btn = new Button();
        btn.setText("Say 'Hello World'");
        btn.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Hello World!");
            }
        });
        
        Button otherBtn = new Button();
        btn.setText("Say 'Goodbye Cruel World'");
        btn.setOnAction(new EventHandler<ActionEvent>() {
            
            @Override
            public void handle(ActionEvent event) {
                System.out.println("Goodbye cruel world!");
            }
        });
        
       //StackPane root = new StackPane();
        FlowPane flow = new FlowPane();
        //GridPane grid = new GridPane();
        
        flow.getChildren().add(btn);
        flow.getChildren().add(otherBtn);
        //grid.add(btn, 0, 0);
        //grid.add(otherBtn, 0, 1);
        
        //root.getChildren().add(btn);
        //root.getChildren().add(otherBtn);
        
        Scene scene = new Scene(flow, 300, 250);
        //Scene scene = new Scene(grid, 300, 250);
        
        primaryStage.setTitle("Hello World!");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
